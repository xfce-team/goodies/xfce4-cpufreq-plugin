# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Charles Monzat <c.monzat@laposte.net>, 2017-2018
# jc1 <jc1.quebecos@gmail.com>, 2013
# jc1 <jc1.quebecos@gmail.com>, 2013,2021-2022
# jc <jc>, 2013
# Wallon Wallon, 2022
# Yannick Le Guen <leguen.yannick@gmail.com>, 2014,2017,2019-2021
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-24 00:51+0100\n"
"PO-Revision-Date: 2022-02-01 22:15+0000\n"
"Last-Translator: jc1 <jc1.quebecos@gmail.com>\n"
"Language-Team: French (http://www.transifex.com/xfce/xfce-panel-plugins/language/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:115
msgid "Select font..."
msgstr "Sélectionner la police…"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:116
msgid "Select font family and size to use for the labels."
msgstr "Sélectionner la famille de police et la taille à utiliser pour les étiquettes."

#: ../panel-plugin/xfce4-cpufreq-configure.cc:121
msgid "Right-click to revert to the default font."
msgstr "Clic droit pour revenir à la police par défaut."

#: ../panel-plugin/xfce4-cpufreq-configure.cc:133
msgid "Select font"
msgstr "Sélectionner la police"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:193
msgid "Right-click to revert to the default color"
msgstr "Clic droit pour revenir à la couleur par défaut."

#: ../panel-plugin/xfce4-cpufreq-configure.cc:310
msgid "Configure CPU Frequency Monitor"
msgstr "Configurer le moniteur de fréquence du processeur"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:313
#: ../panel-plugin/xfce4-cpufreq-overview.cc:242
msgid "_Close"
msgstr "_Fermer"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:334
msgid "<b>Monitor</b>"
msgstr "<b>Moniteur</b>"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:351
msgid "_Update interval:"
msgstr "_Intervalle de mise à jour :"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:369
msgid "<b>Panel</b>"
msgstr "<b>Tableau de bord</b>"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:390
msgid "_Font:"
msgstr "_Police :"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:409
msgid "_Font color:"
msgstr "_Couleur de police :"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:419
msgid "Select font color"
msgstr "Sélectionner une couleur de police"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:434
msgid "_Display CPU:"
msgstr "_Afficher le processeur :"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:450
msgid "min"
msgstr "min"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:451
msgid "avg"
msgstr "moy"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:452
msgid "max"
msgstr "max"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:486
msgid "Unit:"
msgstr "Unité :"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:496
msgid "Auto"
msgstr "Auto"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:497
msgid "GHz"
msgstr "GHz"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:498
msgid "MHz"
msgstr "MHz"

#. check buttons for display widgets in panel
#: ../panel-plugin/xfce4-cpufreq-configure.cc:521
msgid "_Keep compact"
msgstr "_Garder compact"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:526
msgid "Show text in a single _line"
msgstr "Afficher le texte en une seule _ligne"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:531
msgid "Show CPU _icon"
msgstr "Afficher l’_icône du processeur"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:536
msgid "Adjust CPU icon color according to frequency"
msgstr "Ajustez la couleur de l'icône du processeur en fonction de la fréquence"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:541
msgid "Show CPU fre_quency"
msgstr "Afficher la fré_quence du processeur"

#: ../panel-plugin/xfce4-cpufreq-configure.cc:546
msgid "Show CPU _governor"
msgstr "Afficher le _profil du processeur"

#: ../panel-plugin/xfce4-cpufreq-linux.cc:76
msgid ""
"Your system does not support cpufreq.\n"
"The plugin only shows the current cpu frequency"
msgstr "Votre système ne prend pas en charge cpufreq.\nLe greffon affiche seulement la fréquence processeur actuelle."

#: ../panel-plugin/xfce4-cpufreq-overview.cc:79
msgid "Scaling driver:"
msgstr "Pilote d’ajustement :"

#: ../panel-plugin/xfce4-cpufreq-overview.cc:90
msgid "No scaling driver available"
msgstr "Aucun pilote d’ajustement disponible"

#: ../panel-plugin/xfce4-cpufreq-overview.cc:104
msgid "Available frequencies:"
msgstr "Fréquences disponibles :"

#: ../panel-plugin/xfce4-cpufreq-overview.cc:162
msgid "Available governors:"
msgstr "Profils disponibles :"

#: ../panel-plugin/xfce4-cpufreq-overview.cc:189
msgid "Current governor:"
msgstr "Profil actuel :"

#: ../panel-plugin/xfce4-cpufreq-overview.cc:239
msgid "CPU Information"
msgstr "Informations sur le processeur"

#: ../panel-plugin/xfce4-cpufreq-overview.cc:246
msgid "An overview of all the CPUs in the system"
msgstr "Un aperçu de tous les processeurs dans le système"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:106
msgid "current min"
msgstr "min actuel"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:152
msgid "current avg"
msgstr "moy actuelle"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:188
msgid "current max"
msgstr "max actuel"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:554
msgid "No CPU information available."
msgstr "Aucune information disponible sur le processeur."

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:561
#, c-format
msgid "%zu cpu available"
msgid_plural "%zu cpus available"
msgstr[0] "%zu processeur disponible"
msgstr[1] "%zu processeurs disponibles"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:567
msgid "Frequency: "
msgstr "Fréquence :"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:574
msgid "Governor: "
msgstr "Profil :"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:985
#: ../panel-plugin/cpufreq.desktop.in.h:2
msgid "Show CPU frequencies and governor"
msgstr "Afficher les fréquences et le profil du processeur"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:987
msgid "Copyright (c) 2003-2022\n"
msgstr "Copyright (c) 2003-2022\n"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:1009
msgid ""
"Your system is not configured correctly to support CPU frequency scaling!"
msgstr "Votre système n’est pas configuré correctement pour prendre en charge l’ajustement de fréquence du processeur !"

#: ../panel-plugin/xfce4-cpufreq-plugin.cc:1016
msgid "Your system is not supported yet!"
msgstr "Votre système n’est pas encore pris en charge !"

#: ../panel-plugin/xfce4-cpufreq-utils.cc:87
msgid ""
"The CPU displayed by the XFCE cpufreq plugin has been reset to a default "
"value"
msgstr "Le CPU affiché par le greffon Xfce cpufreq a été réinitialisé vers une valeur par défaut"

#: ../panel-plugin/cpufreq.desktop.in.h:1
msgid "CPU Frequency Monitor"
msgstr "Moniteur de fréquence du processeur"
